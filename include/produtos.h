#ifndef PRODUTOS_H
#define PRODUTOS_H

#include <string>
using std::string ;

#include <istream>
using std::istream ;
#include <ostream>
using std::ostream ;
using std::endl ;


class Produtos {
	protected:
		string codigo ;
		string descricao ;
		float preco ;
		int estoque ;

	public:	
		Produtos () ;
		Produtos (string c, string d ,float p, int e) ;
		~Produtos () ;
		void setCodigo (string c) ;
		void setDescricao (string d) ;
		void setPreco (float p) ;
		void setEstoque (int e) ;

} ;

class Bebida: public Produtos {
	private:
		float teor_alcoolico ;
		float acucar ;

	public :
		Bebida() ;
		Bebida (string c, string d ,float p, int e , float ta , float a) ;
		~Bebida () ;
		void setCodigo (string c) ;
		string getCodigo () ;
		void setDescricao (string d) ;
		string getDecricao () ;
		void setPreco (float p) ;
		float getPreco () ;
		void setEstoque (int e) ;
		int getEstoque () ;
		void setTeorAlcoolico (float ta) ;
		float getTeorAlcoolico () ;
		void setAcucar (float a) ;
		float getAcucar () ;
		friend ostream& operator<<(ostream& os, Bebida &b) ;
		friend istream& operator>>(istream& is, Bebida &b) ;

} ;

class Fruta: public Produtos {
	private :
		string numero ;
		string data ;

	public:
		Fruta () ;
		Fruta (string c, string d ,float p, int e ,string n , string da) ;
		~Fruta () ;
		void setCodigo (string c) ;
		void setDescricao (string d) ;
		void setPreco (float p) ;
		void setEstoque (int e) ;
		void setNumero (string n) ;
		void setData (string da) ;
		friend ostream& operator<<(ostream& os, Fruta &f) ;
		friend istream& operator>>(istream& is, Fruta &f) ;
} ;

class Salgado : public Produtos {
	private :
		float sodio ;
		bool gluten ;

	public :
		Salgado () ;
		Salgado (string c, string d ,float p, int e , float s, bool g) ;
		~Salgado () ;
		void setCodigo (string c) ;
		void setDescricao (string d) ;
		void setPreco (float p) ;
		void setEstoque (int e) ;
		void setSodio (float s) ;
		void setGluten (bool g) ;
		friend ostream& operator<<(ostream& os, Salgado &s) ;
		friend istream& operator>>(istream& is, Salgado &s) ;

} ;

class Doce: public Produtos {
	private :
		float acucar ;
		bool gluten ;

	public :
		Doce () ;
		Doce (string c , string d , float p , int e , float s , bool g) ;
	 	~Doce () ;
	 	void setCodigo (string c) ;
		void setDescricao (string d) ;
		void setPreco (float p) ;
		void setEstoque (int e) ;
		void setAcucar (float a) ;
		void setGluten (bool g) ; 
		friend ostream& operator<<(ostream& os, Doce &d) ;
		friend istream& operator>>(istream& is, Doce &d) ; 
} ;

class Cd: public Produtos {
	private: 
		string estilo ;
		string artista ;
		string nome_album ;

	public :
		Cd () ;
		Cd (string c , string d , float p , int e, string es , string a ,string nb) ;
		~Cd () ;
		void setCodigo (string c) ;
		void setDescricao (string d) ;
		void setPreco (float p) ;
		void setEstoque (int e) ;
		void setEstilo (string es) ;
		void setArtista (string a) ;
		void setNomeAlbum (string nb ) ; 
		friend ostream& operator<<(ostream& os, Cd &c) ;
		friend istream& operator>>(istream& is, Cd &c) ;
} ;

class Dvd : public Produtos {
	private:
		string titulo ;
		string genero ;
		int minutos ;

	public :
		Dvd () ;
		Dvd (string c , string d , float p , int e , string t , string g , int m) ;
		~Dvd () ;
		void setCodigo (string c) ;
		void setDescricao (string d) ;
		void setPreco (float p) ;
		void setEstoque (int e) ;
		void setTitulo (string t) ;
		void setGenero (string g) ;
		void setMinutos (int m) ;
		friend ostream& operator<<(ostream& os, Dvd &d) ;
		friend istream& operator>>(istream& is, Dvd &d) ;
} ;


class Livro: public Produtos {
	private :
		string titulo ;
		string autor ;
		string editora ;
		string ano_publicacao ;

	public :
		Livro () ;
		Livro (string c , string d , float p , int e , string t , string a , string ed , string ap) ;
		~Livro () ;
		void setCodigo (string c) ;
		void setDescricao (string d) ;
		void setPreco (float p) ;
		void setEstoque (int e) ;
		void setTitulo (string t) ;
		void setAutor (string a) ;
		void setEditora (string ed) ;
		void setAnoPublicacao (string ap) ;
		friend ostream& operator<<(ostream& os, Livro &l) ;
		friend istream& operator>>(istream& is, Livro &l) ;
} ;





#endif  /* PRODUTOS_H */