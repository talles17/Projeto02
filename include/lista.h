#ifndef LISTA_H
#define LISTA_H

#include <iostream>
using std::cout ;
using std::endl ;

template <class T>
class ListaLigada {
	
	struct Node {
		T data ;
		Node *prox ;
	} ;

	public:
		ListaLigada () ;
		void add (T d) ;
		void Delete (T d) ;
		void printList () ;
		int tamanho () ;

	private:
		Node *head ;
		Node *curr ;
		Node *temp ;

} ;

template <class T>
ListaLigada<T>::ListaLigada () {
	head = NULL ;
	curr = NULL ;
	temp = NULL ;
}

template <class T>
void ListaLigada<T>::add (T d) {
	Node *n = new Node() ;
	n->prox = NULL ;
	n->data = d ;

	if (head != NULL ) {
		curr = head ;
		while (curr->prox != NULL ) {
			curr = curr->prox ;
		}
		curr->prox = n ;
	}
	else {
		head = n ;
	}
}

template <class T>
void ListaLigada<T>::Delete (T d) {
	Node *n = NULL ;
	temp = head ;
	curr = head ;
	while (curr != NULL && curr->data != d) {
		temp = curr ;
		curr = curr->prox ;
	}
	if (curr = NULL) {
		cout << d << " Nao esta na lista." << endl ;
		delete n ;
	}
	else {
		n = curr ;
		curr = curr->prox ;
		temp->prox = curr ;
		delete n ;
		cout << "O " << d << " foi deletado da lista." << endl ;
	}
}

template<class T>
void ListaLigada<T>::printList () {
	curr = head ;
	while (curr != NULL) {
		cout << curr->data << endl ;
		curr = curr->prox ;
	}
}

template <class T>
int ListaLigada<T>::tamanho () {
	int t = 0 ;
	curr = head ;
	while (curr != NULL) {
		curr = curr ->prox ;
		t++ ;
	}

	return t ;
}

#endif  /* LISTA_H */