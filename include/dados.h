#ifndef DADOS_H
#define DADOS_H

#include "produtos.h"
#include "lista.h"

void carregadados_bebidas (class ListaLigada<Bebida> &lista ) ;

void carregadados_frutas (class ListaLigada<Fruta> &lista) ;

void carregadados_salgados (class ListaLigada<Salgado> &lista) ;

void carregadados_doces (class ListaLigada<Doce> &lista) ;

void carregadados_cds (class ListaLigada<Cd> &lista) ;

void carregadados_dvds (class ListaLigada<Dvd> &lista) ;

void carregadados_livros (class ListaLigada<Livro> &lista) ;



#endif   /* DADOS_H */