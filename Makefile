RM= rm -rf
CC=g++

LIB_DIR=./lib
INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
TEST_DIR=./test


CFLAGS= -Wall -pedantic -ansi -std=c++11 -I. -I$(INC_DIR)

.PHONY: all clean debug 

all: executavel


debug: CFLAGS += -g -O0
debug: executavel

#$(OBJ_DIR)/frutas.o $(OBJ_DIR)/salgados.o $(OBJ_DIR)/dados.o
executavel: $(OBJ_DIR)/produtos.o $(OBJ_DIR)/bebidas.o $(OBJ_DIR)/frutas.o $(OBJ_DIR)/salgados.o $(OBJ_DIR)/doces.o $(OBJ_DIR)/cds.o $(OBJ_DIR)/dvds.o $(OBJ_DIR)/livros.o $(OBJ_DIR)/dados.o $(OBJ_DIR)/registra_dados.o $(OBJ_DIR)/main.o 
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'executavel' criado em $(BIN_DIR)] +++"
	@echo "============="


$(OBJ_DIR)/produtos.o: $(SRC_DIR)/produtos.cpp $(INC_DIR)/produtos.h
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/bebidas.o: $(SRC_DIR)/bebidas.cpp 
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/frutas.o: $(SRC_DIR)/frutas.cpp 
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/salgados.o: $(SRC_DIR)/salgados.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/doces.o: $(SRC_DIR)/doces.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/cds.o: $(SRC_DIR)/cds.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/dvds.o: $(SRC_DIR)/dvds.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/livros.o: $(SRC_DIR)/livros.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/dados.o: $(SRC_DIR)/dados.cpp 
	$(CC) -c $(CFLAGS) -o $@ $<

$(OBJ_DIR)/registra_dados.o: $(SRC_DIR)/registra_dados.cpp
	$(CC) -c $(CFLAGS) -o $@ $< 

$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp $(INC_DIR) $(INC_DIR)/lista.h
	$(CC) -c $(CFLAGS) -o $@ $<



clean:
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*