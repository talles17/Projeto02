#include <iostream>
using std::cerr ;
using std::endl ;
using std::cout ;

#include <fstream>
using std::ifstream ;

#include <string>
using std::string ;
using std::getline ;


#include "dados.h"
#include "produtos.h"
#include "lista.h"


void carregadados_bebidas (class ListaLigada<Bebida> &lista) {
	ifstream entrada ("data/bebidas.csv") ; 
	if (entrada.bad() || !entrada || (entrada.is_open() == 0)) {
		cerr << "O arquivo nao abriu corretamente" << endl ;
		cerr << "O programa sera encerrado" << endl ;
		exit (1) ;
	}

	int produtos = 0 ;
	string linha ;
	while (getline (entrada, linha)) {
		produtos++ ;
	}

	//cout << produtos << endl ;
	Bebida* bebidas = new Bebida[produtos] ;

	entrada.clear() ;
	entrada.seekg(0 , entrada.beg) ;


	string c , d;
	float p , ta , a ;
	int e ;
	for (int i = 0 ; i < produtos ; i++) {
		getline (entrada, c , ';' ) ;
		bebidas[i].setCodigo(c) ;
		
		getline (entrada, d , ';' ) ;
		bebidas[i].setDescricao (d) ;
		
		entrada >> p ;
		entrada.ignore (';') ;
		bebidas[i].setPreco (p) ;
		
		entrada >> e ;
		entrada.ignore (';') ;
		bebidas[i].setEstoque (e) ;
		
		entrada >> ta ;
		entrada.ignore (';') ;
		bebidas[i].setTeorAlcoolico (ta) ;
		
		entrada >> a ;
		entrada.ignore () ;
		bebidas[i].setAcucar (a) ;

		lista.add (bebidas[i]) ;
		
	} 

	/*for (int i =  0 ; i < produtos ; i++) {
		cout << bebidas[i] << endl ;
	}*/

	entrada.close() ;

	delete [] bebidas ;
	bebidas = NULL ;
}

void carregadados_frutas (class ListaLigada<Fruta> &lista) {
	ifstream entrada ("data/frutas.csv") ; 
	if (entrada.bad() || !entrada || (entrada.is_open() == 0)) {
		cerr << "O arquivo nao abriu corretamente" << endl ;
		cerr << "O programa sera encerrado" << endl ;
		exit (1) ;
	}

	int produtos = 0 ;
	string linha ;
	while (getline (entrada, linha)) {
		produtos++ ;
	}

	//cout << produtos << endl ;
	Fruta* frutas = new Fruta[produtos] ;

	entrada.clear() ;
	entrada.seekg(0 , entrada.beg) ;


	string c , d , n , da;
	float p ;
	int e ;
	for (int i = 0 ; i < produtos ; i++) {
		getline (entrada, c , ';' ) ;
		frutas[i].setCodigo(c) ;
		
		getline (entrada, d , ';' ) ;
		frutas[i].setDescricao (d) ;
		
		entrada >> p ;
		entrada.ignore (';') ;
		frutas[i].setPreco (p) ;
		
		entrada >> e ;
		entrada.ignore (';') ;
		frutas[i].setEstoque (e) ;
		
		getline (entrada , n , ';') ;
		frutas[i].setNumero (n) ;
		
		getline (entrada , da , ';') ;
		frutas[i].setData (da) ;

		lista.add (frutas[i]) ;
		
	} 

	/*for (int i =  0 ; i < produtos ; i++) {
		cout << bebidas[i] << endl ;
	}*/

	entrada.close() ;

	delete [] frutas ;
	frutas = NULL ;
}

void carregadados_salgados (class ListaLigada<Salgado> &lista) {
	ifstream entrada ("data/salgados.csv") ; 
	if (entrada.bad() || !entrada || (entrada.is_open() == 0)) {
		cerr << "O arquivo nao abriu corretamente" << endl ;
		cerr << "O programa sera encerrado" << endl ;
		exit (1) ;
	}

	int produtos = 0 ;
	string linha ;
	while (getline (entrada, linha)) {
		produtos++ ;
	}

	//cout << produtos << endl ;
	Salgado* salgados = new Salgado[produtos] ;

	entrada.clear() ;
	entrada.seekg(0 , entrada.beg) ;


	string c , d ;
	float p , s ;
	int e ;
	bool g ;
	for (int i = 0 ; i < produtos ; i++) {
		getline (entrada, c , ';' ) ;
		salgados[i].setCodigo(c) ;
		
		getline (entrada, d , ';' ) ;
		salgados[i].setDescricao (d) ;
		
		entrada >> p ;
		entrada.ignore (';') ;
		salgados[i].setPreco (p) ;
		
		entrada >> e ;
		entrada.ignore (';') ;
		salgados[i].setEstoque (e) ;
		
		entrada >> s ;
		salgados[i].setSodio (s) ;
		
		entrada >> g ;
		salgados[i].setGluten (g) ;

		lista.add (salgados[i]) ;
		
	} 

	entrada.close() ;

	delete [] salgados ;
	salgados = NULL ;
}

void carregadados_doces (class ListaLigada<Doce> &lista) {
	ifstream entrada ("data/doces.csv") ; 
	if (entrada.bad() || !entrada || (entrada.is_open() == 0)) {
		cerr << "O arquivo nao abriu corretamente" << endl ;
		cerr << "O programa sera encerrado" << endl ;
		exit (1) ;
	}

	int produtos = 0 ;
	string linha ;
	while (getline (entrada, linha)) {
		produtos++ ;
	}

	//cout << produtos << endl ;
	Doce* doces = new Doce[produtos] ;

	entrada.clear() ;
	entrada.seekg(0 , entrada.beg) ;


	string c , d ;
	float p , a ;
	int e ;
	bool g ;
	for (int i = 0 ; i < produtos ; i++) {
		getline (entrada, c , ';' ) ;
		doces[i].setCodigo(c) ;
		
		getline (entrada, d , ';' ) ;
		doces[i].setDescricao (d) ;
		
		entrada >> p ;
		entrada.ignore (';') ;
		doces[i].setPreco (p) ;
		
		entrada >> e ;
		entrada.ignore (';') ;
		doces[i].setEstoque (e) ;
		
		entrada >> a ;
		doces[i].setAcucar (a) ;
		
		entrada >> g ;
		doces[i].setGluten (g) ;

		lista.add (doces[i]) ;
		
	} 

	entrada.close() ;

	delete [] doces ;
	doces = NULL ;
}

void carregadados_cds (class ListaLigada<Cd> &lista) {
	ifstream entrada ("data/cds.csv") ; 
	if (entrada.bad() || !entrada || (entrada.is_open() == 0)) {
		cerr << "O arquivo nao abriu corretamente" << endl ;
		cerr << "O programa sera encerrado" << endl ;
		exit (1) ;
	}

	int produtos = 0 ;
	string linha ;
	while (getline (entrada, linha)) {
		produtos++ ;
	}

	//cout << produtos << endl ;
	Cd* cds = new Cd[produtos] ;

	entrada.clear() ;
	entrada.seekg(0 , entrada.beg) ;


	string c , d , es , a , nb ;
	float p ;
	int e ;
	for (int i = 0 ; i < produtos ; i++) {
		getline (entrada, c , ';' ) ;
		cds[i].setCodigo(c) ;
		
		getline (entrada, d , ';' ) ;
		cds[i].setDescricao (d) ;
		
		entrada >> p ;
		entrada.ignore (';') ;
		cds[i].setPreco (p) ;
		
		entrada >> e ;
		entrada.ignore (';') ;
		cds[i].setEstoque (e) ;
		
		getline (entrada, es , ';' ) ;
		cds[i].setEstilo (es) ;
		
		getline (entrada, a , ';' ) ;
		cds[i].setArtista (a) ;

		getline (entrada, nb , ';' ) ;
		cds[i].setNomeAlbum (nb) ;

		lista.add (cds[i]) ;
		
	} 

	entrada.close() ;

	delete [] cds ;
	cds = NULL ;
}

void carregadados_dvds (class ListaLigada<Dvd> &lista) {
	ifstream entrada ("data/dvds.csv") ; 
	if (entrada.bad() || !entrada || (entrada.is_open() == 0)) {
		cerr << "O arquivo nao abriu corretamente" << endl ;
		cerr << "O programa sera encerrado" << endl ;
		exit (1) ;
	}

	int produtos = 0 ;
	string linha ;
	while (getline (entrada, linha)) {
		produtos++ ;
	}

	//cout << produtos << endl ;
	Dvd* dvds = new Dvd[produtos] ;

	entrada.clear() ;
	entrada.seekg(0 , entrada.beg) ;


	string c , d , t , g;
	float p ;
	int e , m;
	for (int i = 0 ; i < produtos ; i++) {
		getline (entrada, c , ';' ) ;
		dvds[i].setCodigo(c) ;
		
		getline (entrada, d , ';' ) ;
		dvds[i].setDescricao (d) ;
		
		entrada >> p ;
		entrada.ignore (';') ;
		dvds[i].setPreco (p) ;
		
		entrada >> e ;
		entrada.ignore (';') ;
		dvds[i].setEstoque (e) ;
		
		getline (entrada, t , ';' ) ;
		dvds[i].setTitulo (t) ;
		
		getline (entrada, g , ';' ) ;
		dvds[i].setGenero (g) ;

		entrada >> m ;
		dvds[i].setMinutos (m) ;

		lista.add (dvds[i]) ;
		
	} 

	entrada.close() ;

	delete [] dvds ;
	dvds = NULL ;
}

void carregadados_livros (class ListaLigada<Livro> &lista) {
	ifstream entrada ("data/livros.csv") ; 
	if (entrada.bad() || !entrada || (entrada.is_open() == 0)) {
		cerr << "O arquivo nao abriu corretamente" << endl ;
		cerr << "O programa sera encerrado" << endl ;
		exit (1) ;
	}

	int produtos = 0 ;
	string linha ;
	while (getline (entrada, linha)) {
		produtos++ ;
	}

	//cout << produtos << endl ;
	Livro* livros = new Livro[produtos] ;

	entrada.clear() ;
	entrada.seekg(0 , entrada.beg) ;


	string c , d , t , a , ed , ap;
	float p ;
	int e ;
	for (int i = 0 ; i < produtos ; i++) {
		getline (entrada, c , ';' ) ;
		livros[i].setCodigo(c) ;
		
		getline (entrada, d , ';' ) ;
		livros[i].setDescricao (d) ;
		
		entrada >> p ;
		entrada.ignore (';') ;
		livros[i].setPreco (p) ;
		
		entrada >> e ;
		entrada.ignore (';') ;
		livros[i].setEstoque (e) ;
		
		getline (entrada, t , ';' ) ;
		livros[i].setTitulo (t) ;
		
		getline (entrada, a , ';' ) ;
		livros[i].setAutor (a) ;

		getline (entrada, ed , ';' ) ;
		livros[i].setEditora (ed) ;

		getline (entrada, ap , ';') ;
		livros[i].setAnoPublicacao(ap) ;


		lista.add (livros[i]) ;
		
	} 

	entrada.close() ;

	delete [] livros ;
	livros = NULL ;
}