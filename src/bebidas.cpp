
#include <string>
using std::string ;

#include <istream>
using std::istream ;
#include <ostream>
using std::ostream ;
using std::endl ;


#include "produtos.h"



Bebida::Bebida () {

}

Bebida::Bebida (string c, string d ,float p, int e , float ta , float a ) {
	codigo = c ;
	descricao = d ;
	preco = p ;
	estoque = e ;
	teor_alcoolico = ta ;
	acucar = a ;
}

Bebida::~Bebida () {

 }

 void Bebida::setCodigo (string c) {
 	codigo = c ;
 }

 string Bebida::getCodigo () {
 	return codigo ;
 }

 void Bebida::setDescricao (string d) {
 	descricao = d ;
 }

 string Bebida::getDescricao () {
 	return descricao ;
 }

 void Bebida::setPreco (float p) {
 	preco = p ;
 }

float Bebida::getPreco () {
 	return preco ;
 }

 void Bebida::setEstoque (int e ) {
 	estoque = e ;
 }

 int Bebida::getEstoque () {
 	return estoque ;
 }

 void Bebida::setTeorAlcoolico (float ta) {
 	teor_alcoolico = ta ;
 }

 float Bebida::getTeorAlcoolico () {
 	return teor_alcoolico ;
 }

 void Bebida::setAcucar (float a) {
 	acucar = a ;
 }

 float Bebida::getAcucar () {
 	return acucar ;
 }

ostream& operator<<(ostream& os, Bebida &b) {
	os << "Codigo de barras: " << b.codigo << endl ;
	os << "Descricao da bebida: " << b.descricao << endl ;
	os << "Preco: " << b.preco << endl ;
	os << "Estoque: " << b.estoque << endl ;
	os << "Teor alcoolico: " << b.teor_alcoolico << endl ;
	os << "Quantidade de acucar(em miligramas): " << b.acucar << endl ;

	return os ;
}

istream& operator>>(istream& is, Bebida &b) {
	is >> b.codigo >> b.descricao >> b.preco >> b.estoque >> b.teor_alcoolico >> b.acucar ;
	return is ;
}