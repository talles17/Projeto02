#include "produtos.h"



Dvd::Dvd () {

}

Dvd::Dvd (string c , string d , float p , int e , string t , string g , int m) {
	codigo = c ;
	descricao = d ;
	preco = p ;
	estoque = e ;
	titulo = t ;
	genero = g ;
	minutos = m ;
}

Dvd::~Dvd () {

}

void Dvd::setCodigo (string c) {
 	codigo = c ;
 }

 void Dvd::setDescricao (string d) {
 	descricao = d ;
 }

 void Dvd::setPreco (float p) {
 	preco = p ;
 }

 void Dvd::setEstoque (int e ) {
 	estoque = e ;
 }

 void Dvd::setTitulo (string t) {
 	titulo = t ;
 }

 void Dvd::setGenero (string g) {
 	genero = g ;
 }

 void Dvd::setMinutos (int m) {
 	minutos = m ;
 }
 
 ostream& operator<<(ostream& os, Dvd &d) {
	os << "Codigo de barras: " << d.codigo << endl ;
	os << "Descricao da bebida: " << d.descricao << endl ;
	os << "Preco: " << d.preco << endl ;
	os << "Estoque: " << d.estoque << endl ;
	os << "Titulo: " << d.titulo << endl ;
	os << "Genero: " << d.genero << endl ;
	os << "Minutos: " << d.minutos << endl ;

	return os ;
}

istream& operator>>(istream& is, Dvd &d) {
	is >> d.codigo >> d.descricao >> d.preco >> d.estoque >> d.titulo >> d.genero >> d.minutos ;
	return is ;
}