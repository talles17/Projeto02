#include "produtos.h"

Cd::Cd () {

}

Cd::Cd (string c , string d , float p , int e, string es , string a ,string nb) {
	codigo = c ;
	descricao = d ;
	preco = p ;
	estoque = e ;
	estilo = es ;
	artista = a ;
	nome_album = nb ;
}

Cd::~Cd () {

}

void Cd::setCodigo (string c) {
 	codigo = c ;
 }

 void Cd::setDescricao (string d) {
 	descricao = d ;
 }

 void Cd::setPreco (float p) {
 	preco = p ;
 }

 void Cd::setEstoque (int e ) {
 	estoque = e ;
 }

 void Cd::setEstilo (string es) {
 	estilo = es ;
 }

 void Cd::setArtista (string a) {
 	artista = a ;
 }

 void Cd::setNomeAlbum (string nb) {
 	nome_album = nb ;
 }

 ostream& operator<<(ostream& os, Cd &c) {
	os << "Codigo de barras: " << c.codigo << endl ;
	os << "Descricao da bebida: " << c.descricao << endl ;
	os << "Preco: " << c.preco << endl ;
	os << "Estoque: " << c.estoque << endl ;
	os << "Estilo: " << c.estilo << endl ;
	os << "Artista: " << c.artista << endl ;
	os << "Nome do album: " << c.nome_album << endl ;

	return os ;
}

istream& operator>>(istream& is, Cd &c) {
	is >> c.codigo >> c.descricao >> c.preco >> c.estoque >> c.estilo >> c.artista >> c.nome_album ;
	return is ;
}