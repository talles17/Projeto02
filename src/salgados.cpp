#include <string>
using std::string ;

#include <ostream>
using std::ostream ;
using std::endl ;


#include "produtos.h"




Salgado::Salgado () {
	
}

Salgado::Salgado (string c, string d ,float p, int e , float s, bool g) {
	codigo = c ;
	descricao = d ;
	preco = p ;
	estoque = e ;
	sodio = s ;
	gluten = g ;
}

Salgado::~Salgado () {

}

void Salgado::setCodigo (string c) {
 	codigo = c ;
 }

 void Salgado::setDescricao (string d) {
 	descricao = d ;
 }

 void Salgado::setPreco (float p) {
 	preco = p ;
 }

 void Salgado::setEstoque (int e ) {
 	estoque = e ;
 }

 void Salgado::setSodio (float s) {
 	sodio = s ;
 }

 void Salgado::setGluten (bool g) {
 	gluten = g ;
 }

 ostream& operator<<(ostream& os, Salgado &s) {
	os << "Codigo de barras: " << s.codigo << endl ;
	os << "Descricao da bebida: " << s.descricao << endl ;
	os << "Preco: " << s.preco << endl ;
	os << "Estoque: " << s.estoque << endl ;
	os << "Sodio: " << s.sodio << endl ;
	os << "Gluten: " << s.gluten << endl ;

	return os ;
}

istream& operator>>(istream& is, Salgado &s) {
	is >> s.codigo >> s.descricao >> s.preco >> s.estoque >> s.sodio >> s.gluten ;
	return is ;
}