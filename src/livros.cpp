#include "produtos.h"



Livro::Livro () {

}

Livro::Livro (string c , string d , float p , int e , string t , string a , string ed , string ap) {
	codigo = c ;
	descricao = d ;
	preco = p ;
	estoque = e ;
	titulo = t ;
	autor = a ;
	editora = ed ;
	ano_publicacao = ap ;
}

Livro::~Livro () {

}

void Livro::setCodigo (string c) {
 	codigo = c ;
 }

 void Livro::setDescricao (string d) {
 	descricao = d ;
 }

 void Livro::setPreco (float p) {
 	preco = p ;
 }

 void Livro::setEstoque (int e ) {
 	estoque = e ;
 }

 void Livro::setTitulo (string t) {
 	titulo = t ;
 }

 void Livro::setAutor (string a) {
 	autor = a ;
 }

 void Livro::setEditora (string ed) {
 	editora = ed ;
 }

 void Livro::setAnoPublicacao (string ap) {
 	ano_publicacao = ap ;
 }

ostream& operator<<(ostream& os, Livro &l) {
	os << "Codigo de barras: " << l.codigo << endl ;
	os << "Descricao da bebida: " << l.descricao << endl ;
	os << "Preco: " << l.preco << endl ;
	os << "Estoque: " << l.estoque << endl ;
	os << "Titulo: " << l.titulo << endl ;
	os << "Autor: " << l.autor << endl ;
	os << "Editora: " << l.editora << endl ;
	os << "Ano de publicacao: " << l.ano_publicacao << endl ;

	return os ;
}

istream& operator>>(istream& is, Livro &l) {
	is >> l.codigo >> l.descricao >> l.preco >> l.estoque >> l.titulo >> l.autor >> l.editora >> l.ano_publicacao ;
	return is ;
}