#include "produtos.h"



Doce::Doce () {

}

Doce::Doce (string c , string d , float p , int e , float a , bool g) {
	codigo = c ;
	descricao = d ;
	preco = p ;
	estoque = e ;
	acucar = a ;
	gluten = g ;
}

Doce::~Doce () {

}

void Doce::setCodigo (string c) {
 	codigo = c ;
 }

 void Doce::setDescricao (string d) {
 	descricao = d ;
 }

 void Doce::setPreco (float p) {
 	preco = p ;
 }

 void Doce::setEstoque (int e ) {
 	estoque = e ;
 }

 void Doce::setAcucar (float a) {
 	acucar = a ; 
 }

 void Doce::setGluten (bool g) {
 	gluten = g ;
 }

 ostream& operator<<(ostream& os,Doce &d) {
	os << "Codigo de barras: " << d.codigo << endl ;
	os << "Descricao da bebida: " << d.descricao << endl ;
	os << "Preco: " << d.preco << endl ;
	os << "Estoque: " << d.estoque << endl ;
	os << "Quantidade de acucar(em miligramas): " << d.acucar << endl ;
	os << "Gluten " << d.gluten << endl ;

	return os ;
}

istream& operator>>(istream& is,Doce &d) {
	is >> d.codigo >> d.descricao >> d.preco >> d.estoque >> d.acucar >> d.gluten ;
	return is ;
}