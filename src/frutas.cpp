#include <string>
using std::string ;

#include <ostream>
using std::ostream ;
using std::endl ;

#include "produtos.h"



Fruta::Fruta () {

}

Fruta::Fruta (string c, string d ,float p, int e ,string n, string da) {
	codigo = c ;
	descricao = d ;
	preco = p ;
	estoque = e ;
	numero = n ;
	data = da ;
}

Fruta::~Fruta () {

}

void Fruta::setCodigo (string c) {
 	codigo = c ;
 }

 void Fruta::setDescricao (string d) {
 	descricao = d ;
 }

 void Fruta::setPreco (float p) {
 	preco = p ;
 }

 void Fruta::setEstoque (int e ) {
 	estoque = e ;
 }

 void Fruta::setNumero (string n) {
 	numero = n ;
 }

 void Fruta::setData (string d) {
 	data = d ;
 }

ostream& operator<<(ostream& os, Fruta &f) {
	os << "Codigo de barras: " << f.codigo << endl ;
	os << "Descricao da bebida: " << f.descricao << endl ;
	os << "Preco: " << f.preco << endl ;
	os << "Estoque: " << f.estoque << endl ;
	os << "Numero: " << f.numero << endl ;
	os << "Data: " << f.data << endl ;

	return os ;
}

istream& operator>>(istream& is, Fruta &f) {
	is >> f.codigo >> f.descricao >> f.preco >> f.estoque >> f.numero >> f.data ;
	return is ;
}

 